package com.example.homework6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val people = ArrayList<Person>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    fun init(){
        setData()
        name_List.layoutManager = LinearLayoutManager(this)
        name_List.adapter = NameListAdapter(people)
    }

    fun setData(){
        var person: Person=Person("Shako","Ghlonti")
        var person2: Person=Person("Nika","Ghlonti")
        people.add(person)
        people.add(person2)

    }
}
