package com.example.homework6


@Entity
class Person(name:String,surname:String) {
    @PrimaryKey(autoGenerate = true)
    var uid: Int = 0
    @ColumnInfo(name = "first_name")
    var name:String? = ""
    @ColumnInfo(name = "last_name")
    var surname:String? = ""
}